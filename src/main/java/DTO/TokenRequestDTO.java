package DTO;

/**
 * Token Request DTO
 */
public class TokenRequestDTO {
    /**
     * The token request id
     */
    public String id;

    /**
     * The customer requesting the tokens
     */
    public String customer;

    /**
     * The amount of tokens requested
     */
    public int amount;

    public TokenRequestDTO() {
    }

    public TokenRequestDTO(String id, String customer, int amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

}
