package DTO;

/**
 * Validate Token DTO
 */
public class ValidateTokenDTO {

    /**
     * The token validation request id
     */
    public String id;

    /**
     * The token to validate
     */
    public String token;

    public ValidateTokenDTO() {
    }

    public ValidateTokenDTO(String id, String token) {
        this.id = id;
        this.token = token;
    }

}
