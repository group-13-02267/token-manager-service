package DTO;

/**
 * Token DTO so that only token value is transferred
 */
public class TokenDTO {

    /**
     * The token value
     */
    public String value;

    public TokenDTO() {
    }

    public TokenDTO(String value) {
        this.value = value;
    }

}
