package DTO;

import token.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Data transfer object for a token requested
 *
 * @author David
 */
public class TokenResponseDTO {

    /**
     * The TokenResponse request id
     */
    public String id;

    /**
     * A list of the generated tokens
     */
    public List<TokenDTO> tokens;

    /**
     * The response status.
     * Is false if fx. customer has requested too many tokens etc.
     */
    public boolean status;

    public TokenResponseDTO() {
    }

    public TokenResponseDTO(String id, List<Token> tokens, boolean status) {
        this.id = id;
        this.status = status;

        // Making sure that the tokens received only transmits the values or an empty list
        if (tokens != null) {
            this.tokens = new ArrayList<>(tokens.size());
            for (Token token : tokens) {
                this.tokens.add(new TokenDTO(token.getValue()));
            }
        } else {
            this.tokens = new ArrayList<>(0);
        }
    }

}
