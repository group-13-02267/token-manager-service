package DTO;

/**
 * Validation DTO for validating a token used for refund or payment
 */
public class ValidationDTO {

    /**
     * The validation request id
     */
    public String id;

    /**
     * The validation status
     */
    public boolean status;

    /**
     * The customer that the token belongs to
     */
    public String customer;

    public ValidationDTO() {
    }

    public ValidationDTO(String id, boolean status, String customer) {
        this.id = id;
        this.status = status;
        this.customer = customer;
    }

}
