package messaging;

import DTO.TokenRequestDTO;
import DTO.TokenResponseDTO;
import com.google.gson.Gson;
import core.AsyncService;
import token.Token;
import token.TokenManagerService;

import java.util.List;

/**
 * Token Request Service for requesting new customer tokens
 *
 * @author David
 */
public class TokenRequestService extends AsyncService<TokenRequestDTO> {

    /**
     * The exchange for receiving token requests
     */
    private static final String exchange = "token";

    /**
     * The token manager service responsible for generating tokens etc.
     */
    private final TokenManagerService tokenService;

    /**
     * The message sender to send the generated tokens over
     */
    private MessageSender<TokenResponseDTO> sender;

    /**
     * Instantiate a new TokenRequestService
     *
     * @param tokenService The TokenManagerService responsible for the token generation
     * @param sender       The message sender
     */
    public TokenRequestService(TokenManagerService tokenService, MessageSender<TokenResponseDTO> sender) {
        super(exchange, "token.request.*");
        this.tokenService = tokenService;
        this.sender = sender;
    }

    @Override
    protected TokenRequestDTO handleMessage(String message) {
        return new Gson().fromJson(message, TokenRequestDTO.class);
    }

    @Override
    public void receive(TokenRequestDTO received) {
        List<Token> tokens = tokenService.requestTokens(received.customer, received.amount);

        boolean valid = tokens != null;

        String route = "token.generated." + received.id;

        sender.send(route, new TokenResponseDTO(received.id, tokens, valid));
    }
}
