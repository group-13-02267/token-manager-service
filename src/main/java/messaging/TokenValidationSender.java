package messaging;

import DTO.ValidationDTO;
import core.AsyncSender;
import core.RabbitSender;

/**
 * Token validation sender used to send the token validation result back
 *
 * @author David
 */
public class TokenValidationSender extends MessageSender<ValidationDTO> {

    @Override
    public AsyncSender<ValidationDTO> getSender(String route) {
        return new RabbitSender<>("payment", route);
    }
}
