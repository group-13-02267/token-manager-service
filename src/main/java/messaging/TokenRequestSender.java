package messaging;

import DTO.TokenResponseDTO;
import core.AsyncSender;
import core.RabbitSender;

/**
 * Sender object for the Token Request
 *
 * @author David
 */
public class TokenRequestSender extends MessageSender<TokenResponseDTO> {

    /**
     * The exchange to send back the requested tokens to
     */
    private static final String exchange = "token";

    @Override
    public AsyncSender<TokenResponseDTO> getSender(String route) {
        return new RabbitSender<>(exchange, route);
    }
}

