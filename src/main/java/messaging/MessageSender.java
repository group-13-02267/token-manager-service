package messaging;

import core.AsyncSender;

/**
 * Send a message
 *
 * @param <T> The message type to be sent
 * @author David
 */
public abstract class MessageSender<T> {

    /**
     * Send a message over the message sender
     *
     * @param route   The message route
     * @param message The message to be sent
     */
    public void send(String route, T message) {
        getSender(route).send(message);
    }

    /**
     * Get the sender object used to send messages
     *
     * @param route The message route
     * @return The Asynchronous Sender object
     */
    public abstract AsyncSender<T> getSender(String route);
}
