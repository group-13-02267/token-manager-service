package messaging;

import DTO.ValidateTokenDTO;
import DTO.ValidationDTO;
import com.google.gson.Gson;
import core.AsyncService;
import token.DTUToken;
import token.DTUUserToken;
import token.TokenManagerService;
import token.UserToken;

import java.util.UUID;

/**
 * Token Validation Service for receiving and validating the tokens being used for a payment etc.
 *
 * @author David
 */
public class TokenValidationService extends AsyncService<ValidateTokenDTO> {

    /**
     * The token manager service responsible for validating the tokens
     */
    private final TokenManagerService tokenManagerService;

    /**
     * The message sender used to send back the result
     */
    private MessageSender<ValidationDTO> sender;

    /**
     * Instantiate a new TokenValidationService used to validate tokens
     *
     * @param tokenManagerService The token manager service used to validate the tokens
     * @param sender              The sender object to send the results over
     */
    public TokenValidationService(TokenManagerService tokenManagerService, MessageSender<ValidationDTO> sender) {
        super("payment", "validate.token");
        this.tokenManagerService = tokenManagerService;
        this.sender = sender;
    }

    @Override
    protected ValidateTokenDTO handleMessage(String message) {
        return new Gson().fromJson(message, ValidateTokenDTO.class);
    }

    @Override
    public void receive(ValidateTokenDTO received) {
        sendTokenValidation(received.id, received.token);
    }

    /**
     * Validate, enrich the request with the owning customer and then send back the token validation result
     *
     * @param id    The request id
     * @param token The token to be validated
     */
    private void sendTokenValidation(String id, String token) {
        String route = "token.validate." + id;
        try {
            String customer = tokenManagerService.getUserIdFromToken(new DTUToken(UUID.fromString(token)));

            UserToken userToken = new DTUUserToken(customer, token);

            boolean validated = tokenManagerService.validate(userToken);

            sender.send(route, new ValidationDTO(id, validated, customer));
        } catch (IllegalArgumentException e) {
            sender.send(route, new ValidationDTO(id, false, null));
        }
    }
}
