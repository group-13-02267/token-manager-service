package token;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mathias
 */
public class DTUTokenRepository implements TokenRepository {

    /**
     * The Token Repository storing all the tokens.
     */
    private static DTUTokenRepository instance;

    /**
     * A list of all the generated user tokens
     */
    private List<UserToken> tokens = new ArrayList<>();

    private DTUTokenRepository() {
    }

    /**
     * Get the token repository
     *
     * @return The token repository
     */
    public static TokenRepository getInstance() {
        if (instance == null) {
            instance = new DTUTokenRepository();
        }
        return instance;
    }

    /**
     * Clean the repository of all the tokens
     */
    public static void clean() {
        instance = new DTUTokenRepository();
    }

    public List<UserToken> getAllTokens() {
        return tokens;
    }

    public void storeToken(UserToken token) {
        tokens.add(token);
    }

    public List<UserToken> getUserTokens(String userId) {
        List<UserToken> userTokens = tokens.stream().filter(f -> f.getUserId().equals(userId)).collect(Collectors.toList());
        return userTokens;
    }

    public UserToken getTokenFromValue(String value) {
        UserToken userToken = tokens.stream().filter(f -> f.getValue().equals(value)).findFirst().orElse(null);
        return userToken;
    }

    public void deleteToken(UserToken token) {
        tokens.remove(token);
    }
}
