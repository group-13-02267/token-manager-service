package token;

public interface UserToken extends Token {
    /**
     * Get the user id that the tokens to
     *
     * @return The user id
     */
    String getUserId();
}
