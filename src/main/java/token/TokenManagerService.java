package token;

import java.util.List;

/**
 * Token manager service interface for using, validating and requesting tokens
 */
public interface TokenManagerService {
    /**
     * Use a token
     *
     * @param token The token to be used
     * @return True if the token was used successfully, false if the token is not valid
     */
    boolean useToken(Token token);

    /**
     * Get the user id that the token belongs to
     *
     * @param token The token to get the belonging user
     * @return The user id that the token belongs to
     */
    String getUserIdFromToken(Token token);

    /**
     * Request a new list of tokens
     *
     * @param user  The user id that requests the tokens
     * @param count The amount of tokens to be requested
     * @return A list of the newly generated tokens. Null if the request is not valid.
     */
    List<Token> requestTokens(String user, int count);

    /**
     * Validate the token
     *
     * @param token The token to be validated
     * @return True if token is valid, false if not.
     */
    boolean validate(UserToken token);
}
