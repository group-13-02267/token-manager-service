package token;

/**
 * Interface for all token generations
 *
 * @author Georg
 */
public interface TokenGenerator {
    Token generateToken();
}
