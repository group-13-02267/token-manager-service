package token;

import java.util.Objects;

/**
 * User Token implementation
 *
 * @author Mathias
 */
public class DTUUserToken implements UserToken {

    /**
     * The user id the token belongs to
     */
    private String userId;

    /**
     * The token value
     */
    private String value;

    /**
     * Instantiate a new user token
     * @param userId The user id that the token belongs to
     * @param value The token value
     */
    public DTUUserToken(String userId, String value) {
        this.userId = userId;
        this.value = value;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DTUUserToken that = (DTUUserToken) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, value);
    }
}
