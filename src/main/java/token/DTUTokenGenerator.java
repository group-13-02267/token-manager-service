package token;

import java.util.UUID;

/**
 * Token Generator implementation
 *
 * @author Georg
 */
public class DTUTokenGenerator implements TokenGenerator {

    @Override
    public Token generateToken() {
        UUID uuid = UUID.randomUUID();
        DTUToken token = new DTUToken(uuid);
        return token;
    }
}
