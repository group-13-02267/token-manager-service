package token;

/**
 * Generic token used for refunding and payment etc.
 *
 * @author Georg
 */
public interface Token {

    /**
     * Get the token value
     *
     * @return The token value
     */
    String getValue();

    boolean equals(Object o);
}
