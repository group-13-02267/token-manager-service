package token;

import java.util.ArrayList;
import java.util.List;

/**
 * Token Manager Implementation for validating, generating and generally using tokens
 *
 * @author Georg
 */
public class DTUTokenManagerService implements TokenManagerService {

    /**
     * The token generator
     */
    private final TokenGenerator tokenGenerator;

    /**
     * The token repository storing all the tokens
     */
    private final TokenRepository tokenRepository;

    /**
     * The maximum amount of tokens that can be requested
     */
    private final int mapTokenCount = 5;

    public DTUTokenManagerService(TokenGenerator tokenGenerator, TokenRepository tokenRepository) {
        this.tokenGenerator = tokenGenerator;
        this.tokenRepository = tokenRepository;
    }

    public boolean useToken(Token inputToken) {
        for (UserToken userToken : tokenRepository.getAllTokens()) {
            if (userToken.getValue().equals(inputToken.getValue())) {
                tokenRepository.deleteToken(userToken);
                return true;
            }
        }
        return false;
    }

    public List<Token> requestTokens(String userId, int count) {
        if (count > mapTokenCount) {
            return null;
        }

        if (tokenRepository.getUserTokens(userId).size() > 1) {
            return null;
        }

        List<Token> newTokens = new ArrayList<>();

        for (int c = 0; c < count; c++) {
            Token token = tokenGenerator.generateToken();
            tokenRepository.storeToken(new DTUUserToken(userId, token.getValue()));
            newTokens.add(token);
        }

        return newTokens;
    }

    @Override
    public boolean validate(UserToken token) {
        return useToken(token);
    }

    public String getUserIdFromToken(Token inputToken) {
        UserToken userToken = tokenRepository.getTokenFromValue(inputToken.getValue());
        if (userToken == null) {
            return null;
        }
        return userToken.getUserId();
    }
}
