package token;

import java.util.List;

public interface TokenRepository {

    /**
     * Get all the tokens in the repository
     *
     * @return A list of all the stored tokens
     */
    List<UserToken> getAllTokens();

    /**
     * Store a new token
     *
     * @param token The token to be stored
     */
    void storeToken(UserToken token);

    /**
     * Get all the tokens stored for a specific user
     *
     * @param userId The user id that have the tokens
     * @return A list containing all the tokens that belongs to the user with the user id
     */
    List<UserToken> getUserTokens(String userId);

    /**
     * Delete a specific token from the repository
     *
     * @param token The token to delete
     */
    void deleteToken(UserToken token);

    /**
     * Search for a stored token with the token value
     *
     * @param value The token value to search
     * @return The user token if exists, null if not
     */
    UserToken getTokenFromValue(String value);
}
