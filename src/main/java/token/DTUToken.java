package token;

import java.util.Objects;
import java.util.UUID;

/**
 * Generic DTU Token implementation
 */
public class DTUToken implements Token {

    /**
     * The token value
     */
    private String value;

    public DTUToken(UUID uuid) {
        this.value = uuid.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DTUToken dtuToken = (DTUToken) o;
        return Objects.equals(value, dtuToken.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    /**
     * Get the token value
     *
     * @return The token value
     */
    public String getValue() {
        return this.value;
    }
}
