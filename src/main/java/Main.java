import DTO.TokenRequestDTO;
import DTO.TokenResponseDTO;
import DTO.ValidateTokenDTO;
import DTO.ValidationDTO;
import core.AsyncException;
import core.AsyncListener;
import core.Microservice;
import messaging.*;
import token.*;

/**
 * Main for the TokenManagerService
 *
 * @author David
 */
public class Main extends Microservice {

    private final TokenManagerService tokenService;

    public Main() {
        tokenService = generateManager();
    }

    public static void main(String[] args) throws Exception {
        new Main().startup();
    }

    @Override
    public void startup() throws AsyncException {
        // Token Validation Service for validating tokens
        MessageSender<ValidationDTO> validationSender = new TokenValidationSender();
        AsyncListener<ValidateTokenDTO> validationListener = new TokenValidationService(tokenService, validationSender);
        validationListener.listen();
        // Request Tokens Service for requesting new tokens for a customer
        MessageSender<TokenResponseDTO> requestSender = new TokenRequestSender();
        AsyncListener<TokenRequestDTO> tokenRequest = new TokenRequestService(tokenService, requestSender);
        tokenRequest.listen();
    }

    private TokenManagerService generateManager() {
        TokenGenerator generator = new DTUTokenGenerator();
        TokenRepository repository = DTUTokenRepository.getInstance();
        return new DTUTokenManagerService(generator, repository);
    }
}
