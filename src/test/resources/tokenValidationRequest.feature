# @author Mathias
Feature: Token validation feature

  Scenario: valid token for existing user
    Given a user with 2 tokens
    When the token service receives a valid token to validate
    Then then expires the token
    And  broadcasts that the received token was valid

  Scenario: invalid token for existing user
    Given a user with 2 tokens
    When the token service receives an invalid token to validate
    Then then it does not expire any tokens
    And  broadcasts that the received token was invalid