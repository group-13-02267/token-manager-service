# @author Mathias
Feature: Token request feature

  Scenario: valid token request received
    Given a user with 0 tokens
    When the token service receives a request to generate 4 tokens for that user
    Then it correctly correctly generates the requested amount of tokens
    And broadcasts the tokens

  Scenario: invalid token request received
    Given a user with 6 tokens
    When the token service receives a request to generate 3 tokens for that user
    Then it does not generate any new tokens
    And broadcasts an event indicating that the request was unsucessfull