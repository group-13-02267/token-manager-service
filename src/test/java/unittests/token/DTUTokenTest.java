package unittests.token;

import org.junit.Test;
import token.DTUToken;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class DTUTokenTest {

    @Test
    public void it_holds_the_original_UUID() {
        UUID uuid = UUID.randomUUID();
        DTUToken token = new DTUToken(uuid);

        assertEquals(uuid.toString(), token.getValue());
    }
}
