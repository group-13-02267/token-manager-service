package unittests.token;

import org.junit.Test;
import token.DTUTokenGenerator;
import token.Token;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class DTUTokenGeneratorTest {

    @Test
    public void it_can_generate_a_token() {
        DTUTokenGenerator tokenGenerator = new DTUTokenGenerator();

        Token token = tokenGenerator.generateToken();

        assertNotNull(token);
    }

    @Test
    public void it_generates_new_tokens() {
        DTUTokenGenerator tokenGenerator = new DTUTokenGenerator();

        Token token1 = tokenGenerator.generateToken();
        Token token2 = tokenGenerator.generateToken();

        assertNotEquals(token1, token2);
    }
}
