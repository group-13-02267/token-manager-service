package unittests.token;

import org.junit.Before;
import org.junit.Test;
import token.*;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Primary: Nick, secondary Mathias
 */
public class DTUTokenManagerTest {

    String validuserCpr = "valid-cpr-number";

    TokenGenerator tokenGenerator;

    TokenRepository tokenRepository;

    TokenManagerService tokenManager;

    @Before
    public void setup() {
        tokenGenerator = mock(TokenGenerator.class);
        tokenRepository = mock(TokenRepository.class);
        tokenManager = new DTUTokenManagerService(tokenGenerator, tokenRepository);
    }

    @Test
    public void request_whenZeroActiveTokensForUser() throws Exception {
        int count = 4;
        when(tokenGenerator.generateToken()).thenReturn(new DTUToken(UUID.randomUUID()));
        List<Token> tokens = tokenManager.requestTokens(validuserCpr, count);
        assertNotNull(tokens);
        assertEquals(tokens.size(), count);
    }

    @Test
    public void it_should_allow_to_generate_tokens_when_one_token_left() throws Exception {
        when(tokenGenerator.generateToken()).thenReturn(new DTUToken(UUID.randomUUID()));
        List<Token> tokens = tokenManager.requestTokens(validuserCpr, 1);
        assertEquals(1, tokens.size());

        List<Token> newTokens = tokenManager.requestTokens(validuserCpr, 5);
        assertNotNull(newTokens);
    }

    @Test()
    public void it_should_not_allow_to_generate_more_than_five_tokens_at_a_time() throws Exception {
        List<Token> newTokens = tokenManager.requestTokens(validuserCpr, 6);
        assertNull(newTokens);
    }


    @Test
    public void validTokenShouldUseShouldReturnTrue() {
        String user = "valid-cpr-number";
        Map<String, List<Token>> tokenMap = new HashMap<>();
        List<Token> tokens = Arrays.asList(new DTUToken(UUID.randomUUID()));
        List<UserToken> userTokens = Arrays.asList(new DTUUserToken(user, tokens.get(0).getValue()));

        when(tokenRepository.getUserTokens(user)).thenReturn(userTokens);
        when(tokenRepository.getAllTokens()).thenReturn(userTokens);

        assertTrue(tokenManager.useToken(tokens.get(0)));
    }

    @Test
    public void invalidTokenUseShouldReturnFalse() {
        String user = "valid-cpr-number";
        List<UserToken> userTokens = Arrays.asList(new DTUUserToken(user, UUID.randomUUID().toString()));
        when(tokenRepository.getUserTokens(user)).thenReturn(userTokens);
        when(tokenRepository.getAllTokens()).thenReturn(userTokens);

        assertFalse(tokenManager.useToken(new DTUToken(UUID.randomUUID())));
    }

    @Test
    public void getUserIdFromTokenReturnsCorrectUserId() {
        String user1 = "valid-cpr-1";
        String user2 = "valid-cpr-2";
        DTUToken sampleTokenUser1 = new DTUToken(UUID.randomUUID());
        DTUToken sampleTokenUser2 = new DTUToken(UUID.randomUUID());

        when(tokenRepository.getTokenFromValue(sampleTokenUser1.getValue())).thenReturn(new DTUUserToken(user1, sampleTokenUser1.getValue()));
        when(tokenRepository.getTokenFromValue(sampleTokenUser2.getValue())).thenReturn(new DTUUserToken(user2, sampleTokenUser2.getValue()));

        assertEquals(user1, tokenManager.getUserIdFromToken(sampleTokenUser1));
        assertEquals(user2, tokenManager.getUserIdFromToken(sampleTokenUser2));
    }

    @Test
    public void getUserIdFromTokenReturnsNullOnUnknownToken() {
        assertNull(tokenManager.getUserIdFromToken(new DTUToken(UUID.randomUUID())));
    }

    @Test
    public void valid_whenValidatingTokenForCustomer_shouldBeValid() {
        String user = "user-id";
        String tokenString = UUID.randomUUID().toString();
        UserToken token = new DTUUserToken(user, tokenString);

        List<UserToken> userTokens = new ArrayList<>();
        userTokens.add(token);

        when(tokenRepository.getAllTokens()).thenReturn(userTokens);

        boolean valid = tokenManager.validate(token);

        assertTrue(valid);
    }

    @Test
    public void valid_whenValidatingTokenForNoneExistingCustomer_shouldBeInValid() {
        String user = "user-id";
        String tokenString = UUID.randomUUID().toString();
        UserToken token = new DTUUserToken(user, tokenString);

        when(tokenRepository.getUserTokens(user)).thenReturn(null);

        boolean valid = tokenManager.validate(token);

        assertFalse(valid);
    }

    @Test
    public void valid_whenValidatingTokenForCustomerWithInvalidTokens_shouldBeInValid() {
        String user = "user-id";
        String tokenString1 = UUID.randomUUID().toString();
        String tokenString2 = UUID.randomUUID().toString();
        UserToken token1 = new DTUUserToken(user, tokenString1);
        UserToken token2 = new DTUUserToken(user, tokenString2);

        List<UserToken> userTokens = new ArrayList<>();
        userTokens.add(token1);
        when(tokenRepository.getUserTokens(user)).thenReturn(userTokens);

        boolean valid = tokenManager.validate(token2);

        assertFalse(valid);
    }

    @Test
    public void valid_whenValidatingTokensForCustomerWithNoTokens_shouldBeValid() {
        String user = "user-id";
        String tokenString = UUID.randomUUID().toString();
        UserToken token = new DTUUserToken(user, tokenString);

        List<UserToken> userTokens = new ArrayList<>();
        when(tokenRepository.getUserTokens(user)).thenReturn(userTokens);

        boolean valid = tokenManager.validate(token);

        assertFalse(valid);
    }
}
