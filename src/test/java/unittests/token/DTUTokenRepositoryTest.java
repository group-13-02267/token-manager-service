package unittests.token;

import org.junit.After;
import org.junit.Test;
import token.DTUTokenRepository;
import token.DTUUserToken;
import token.TokenRepository;
import token.UserToken;

import java.util.List;

import static org.junit.Assert.*;

public class DTUTokenRepositoryTest {
    private TokenRepository repository = DTUTokenRepository.getInstance();

    @After
    public void after() {
        DTUTokenRepository.clean();
    }

    @Test
    public void storeToken_whenStoringToken_shouldBeRetrievable() {
        String userId = "user-id";
        UserToken token = new DTUUserToken(userId, "value");
        repository.storeToken(token);
        assertNotNull(repository.getUserTokens(userId));
    }

    @Test
    public void getAllTokens_whenGettingAllTokensWithMultipleUsers_shouldHaveThemAll() {
        UserToken token1 = new DTUUserToken("customer", "token1");
        UserToken token2 = new DTUUserToken("customer2", "token2");
        repository.storeToken(token1);
        repository.storeToken(token2);
        List<UserToken> tokens = repository.getAllTokens();
        assertTrue(tokens.contains(token1));
        assertTrue(tokens.contains(token2));
    }

    @Test
    public void getAllTokens_whenGettingAllTokensWithAnEmptyRepo_shouldReturnEmptyList() {
        List<UserToken> tokens = repository.getAllTokens();
        assertEquals(0, tokens.size());
    }

    @Test
    public void getUserTokens_whenGettingUserTokensWithMultipleUsers_shouldOnlyHaveTheUsers() {
        UserToken token1 = new DTUUserToken("customer", "token1");
        UserToken token2 = new DTUUserToken("customer", "token2");
        UserToken token3 = new DTUUserToken("customer2", "token3");
        repository.storeToken(token1);
        repository.storeToken(token2);
        repository.storeToken(token3);
        List<UserToken> tokens = repository.getUserTokens("customer");
        assertTrue(tokens.contains(token1));
        assertTrue(tokens.contains(token2));
        assertFalse(tokens.contains(token3));
    }

    @Test
    public void getUserTokens_whenGettingUserWithNoTokens_shouldReturnEmptyList() {
        List<UserToken> tokens = repository.getUserTokens("customer");
        assertEquals(0, tokens.size());
    }

    @Test
    public void getTokenByValue_whenGettingToken_shouldReturnToken() {
        UserToken token1 = new DTUUserToken("customer", "token1");
        repository.storeToken(token1);
        assertNotNull(repository.getTokenFromValue("token1"));
    }

    @Test
    public void getTokenByValue_whenGettingTokenThatDoesNotExist_shouldReturnNull() {
        assertNull(repository.getTokenFromValue("token1"));
    }

    @Test
    public void deleteToken_whenDeletingExistingToken_shouldNotExist() {
        UserToken token1 = new DTUUserToken("customer", "token1");
        repository.storeToken(token1);
        assertNotNull(repository.getTokenFromValue("token1"));
        repository.deleteToken(token1);
        assertNull(repository.getTokenFromValue("token1"));
    }

    @Test
    public void deleteToken_whenDeletingNoneExistingToken_shouldJustDelete() {
        assertNull(repository.getTokenFromValue("token1"));
        repository.deleteToken(new DTUUserToken("customer", "token1"));
    }
}
