package unittests.DTO;

import DTO.TokenDTO;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TokenDTOTest {

    @Test
    public void it_has_a_value()
    {
        TokenDTO tokenDTO = new TokenDTO("some-valid-token-string");

        assertEquals("some-valid-token-string", tokenDTO.value);
    }
}
