package unittests.messaging;

import DTO.TokenResponseDTO;
import core.AsyncSender;
import messaging.MessageSender;
import messaging.TokenRequestSender;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class TokenRequestSenderTest {

    private MessageSender<TokenResponseDTO> sender;

    @Before
    public void setup() {
        sender = new TokenRequestSender();
    }

    @Test
    public void getSender_whenGettingSender_shouldReturnCorrectSender() {
        assertThat(sender.getSender("routing"), instanceOf(AsyncSender.class));
    }


}
