package unittests.messaging;

import DTO.ValidateTokenDTO;
import DTO.ValidationDTO;
import core.AsyncService;
import messaging.MessageSender;
import messaging.TokenValidationService;
import org.junit.Before;
import org.junit.Test;
import token.DTUToken;
import token.TokenManagerService;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class TokenValidationServiceTest {

    private MessageSender<ValidationDTO> sender;

    private TokenManagerService tokenManagerService;

    private AsyncService<ValidateTokenDTO> tokenService;

    @Before
    public void setup() {
        sender = mock(MessageSender.class);
        tokenManagerService = mock(TokenManagerService.class);
        tokenService = new TokenValidationService(tokenManagerService, sender);
    }

    @Test
    public void receive_whenReceivingTokenRequest_shouldRequestTokensAndSendMessage() {
        ValidateTokenDTO request = new ValidateTokenDTO("id", UUID.randomUUID().toString());
        tokenService.receive(request);

        verify(tokenManagerService).getUserIdFromToken(any(DTUToken.class));
        verify(sender).send(eq("token.validate." + request.id), any(ValidationDTO.class));
    }

    @Test
    public void receive_whenReceivingTokenRequestWithInvalidUUID_throwException() {
        String token = "invalid-uuid";

        ValidateTokenDTO request = new ValidateTokenDTO("id", token);

        when(tokenManagerService.getUserIdFromToken(any(DTUToken.class)))
                .thenThrow(new IllegalArgumentException());

        tokenService.receive(request);

        verify(sender).send(eq("token.validate." + request.id), any(ValidationDTO.class));
    }
}
