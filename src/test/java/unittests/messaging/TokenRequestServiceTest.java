package unittests.messaging;

import DTO.TokenRequestDTO;
import DTO.TokenResponseDTO;
import core.AsyncService;
import messaging.MessageSender;
import org.junit.Before;
import org.junit.Test;
import token.TokenManagerService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TokenRequestServiceTest {

    private MessageSender<TokenResponseDTO> sender;

    private TokenManagerService tokenManagerService;

    private AsyncService<TokenRequestDTO> tokenService;

    @Before
    public void setup() {
        sender = mock(MessageSender.class);
        tokenManagerService = mock(TokenManagerService.class);
        tokenService = new messaging.TokenRequestService(tokenManagerService, sender);
    }

    @Test
    public void receive_whenReceivingTokenRequest_shouldRequestTokensAndSendMessage() {
        TokenRequestDTO request = new TokenRequestDTO("id", "customer", 5);

        tokenService.receive(request);

        verify(tokenManagerService).requestTokens(request.customer, request.amount);
        verify(sender).send(eq("token.generated." + request.id), any(TokenResponseDTO.class));
    }
}
