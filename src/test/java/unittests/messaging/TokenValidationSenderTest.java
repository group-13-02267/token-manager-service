package unittests.messaging;

import DTO.ValidationDTO;
import core.AsyncSender;
import messaging.MessageSender;
import messaging.TokenValidationSender;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class TokenValidationSenderTest {
    private MessageSender<ValidationDTO> sender;

    @Before
    public void setup() {
        sender = new TokenValidationSender();
    }

    @Test
    public void getSender_whenGettingSender_shouldReturnCorrectSender() {
        assertThat(sender.getSender("routing"), instanceOf(AsyncSender.class));
    }
}
