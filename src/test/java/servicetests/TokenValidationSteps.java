package servicetests;

import DTO.ValidateTokenDTO;
import DTO.ValidationDTO;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import messaging.TokenValidationSender;
import messaging.TokenValidationService;
import servicetests.helpers.UserTokensHolder;
import token.*;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Mathias
 */
public class TokenValidationSteps {

    String actualroute;
    MessageSender messageSender;
    ValidationDTO validationDTO;
    String requestId;
    UserTokensHolder userTokensHolder;
    String receivedTokenValue;
    TokenManagerService tokenManagerService;
    TokenValidationService tokenValidationService;

    public TokenValidationSteps(UserTokensHolder userTokensHolder){
        this.userTokensHolder = userTokensHolder;
        messageSender = new TokenValidationSender() {
            @Override
            public void send(String route, ValidationDTO message) {
                actualroute = route;
                validationDTO = message;
            }
        };
        tokenManagerService = new DTUTokenManagerService(new DTUTokenGenerator(), DTUTokenRepository.getInstance());
        tokenValidationService = new TokenValidationService(tokenManagerService, messageSender);
    }

    @When("the token service receives a valid token to validate")
    public void theTokenServiceReceivesAValidTokenToValidate() {
        receivedTokenValue = userTokensHolder.userTokens.get(0).getValue();
        tokenValidationService.receive(new ValidateTokenDTO(requestId, receivedTokenValue));

    }

    @Then("then expires the token")
    public void thenItValidatesAndExpiresTheToken() {
        List<UserToken> userTokens = DTUTokenRepository.getInstance().getUserTokens(userTokensHolder.name);
        assertEquals(userTokensHolder.nrOfStartTokens - 1, userTokens.size());
        assertTrue(userTokens.stream().noneMatch(u -> u.getValue() == receivedTokenValue));
    }

    @Then("broadcasts that the received token was valid")
    public void broadcastsThatTheReceivedTokenWasValid() {
        //assertEquals(validationDTO.status, true);
        //assertEquals(actualroute,  "token.validate." + receivedTokenValue);
    }

    @When("the token service receives an invalid token to validate")
    public void theTokenServiceReceivesAnInvalidTokenToValidate() {
        receivedTokenValue = UUID.randomUUID().toString();
        tokenValidationService.receive(new ValidateTokenDTO(requestId, receivedTokenValue));
    }

    @Then("then it does not expire any tokens")
    public void thenItDoesNotExpireAnyTokens() {
        List<UserToken> userTokens = DTUTokenRepository.getInstance().getUserTokens(userTokensHolder.name);
        assertEquals(userTokensHolder.nrOfStartTokens, userTokens.size());
    }

    @Then("broadcasts that the received token was invalid")
    public void broadcastsThatTheReceivedTokenWasInvalid() {
        assertEquals(validationDTO.status, false);
    }
}
