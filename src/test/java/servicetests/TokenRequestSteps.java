package servicetests;

import DTO.TokenRequestDTO;
import DTO.TokenResponseDTO;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import messaging.TokenRequestSender;
import messaging.TokenRequestService;
import servicetests.helpers.UserTokensHolder;
import token.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import static org.junit.Assert.*;


/**
 * @author Mathias
 */
public class TokenRequestSteps {
    private int nrOfTokensRequested;
    MessageSender messageSender;
    private String requestId = "someId";
    private String actualroute;
    private TokenResponseDTO tokenResponseDTO;
    private UserTokensHolder userTokensHolder;

    public TokenRequestSteps(UserTokensHolder userTokensHolder){
        this.userTokensHolder = userTokensHolder;
        messageSender = new TokenRequestSender() {
            @Override
            public void send(String route, TokenResponseDTO message) {
                actualroute = route;
                tokenResponseDTO = message;
            }
        };
    }

    @Given("a user with {int} tokens")
    public void aUserWithTokens(Integer nrOfTokens) {
        userTokensHolder.nrOfStartTokens = nrOfTokens;
        userTokensHolder.name = "user1";
        List<DTUUserToken> userTokens = new ArrayList<>();
        for (int i = 0; i < nrOfTokens; i++){
            DTUUserToken userToken = new DTUUserToken(userTokensHolder.name, UUID.randomUUID().toString());
            DTUTokenRepository.getInstance().storeToken(userToken);
            userTokens.add(userToken);  // used to send the users start tokens accoss step files
        }
        userTokensHolder.userTokens = userTokens;
    }

    @When("the token service receives a request to generate {int} tokens for that user")
    public void theTokenServiceReceivesARequestToGenerateTokensForThatUser(Integer nrOfTokensToGenerate) {
        TokenRequestDTO tokenRequestDTO = new TokenRequestDTO(requestId, userTokensHolder.name,nrOfTokensToGenerate);
        nrOfTokensRequested = nrOfTokensToGenerate;

        TokenManagerService tokenManagerService = new DTUTokenManagerService(new DTUTokenGenerator(), DTUTokenRepository.getInstance());
        TokenRequestService tokenRequestService = new TokenRequestService(tokenManagerService, messageSender);

        tokenRequestService.receive(tokenRequestDTO);
    }

    @Then("it correctly correctly generates the requested amount of tokens")
    public void itCorrectlyCorrectlyGeneratesTheRequestedAmountOfTokens() {
        List<UserToken> userTokens = DTUTokenRepository.getInstance().getUserTokens(userTokensHolder.name);
        assertEquals(nrOfTokensRequested - userTokensHolder.nrOfStartTokens, userTokens.size());
    }

    @Then("broadcasts the tokens")
    public void broadcastsTheTokens() {
        assertEquals(actualroute, "token.generated." + requestId);
        assertTrue(tokenResponseDTO.status);
        assertEquals(tokenResponseDTO.tokens.size(), userTokensHolder.nrOfStartTokens + nrOfTokensRequested);
        assertEquals(tokenResponseDTO.id, requestId);
    }

    @Then("it does not generate any new tokens")
    public void itDoesNotGenerateAnyNewTokens() {
        List<UserToken> userTokens = DTUTokenRepository.getInstance().getUserTokens(userTokensHolder.name);
        assertEquals(userTokensHolder.nrOfStartTokens, userTokens.size());
    }

    @Then("broadcasts an event indicating that the request was unsucessfull")
    public void broadcastsAnEventIndicatingThatTheRequestWasUnsucessfull() {
       assertFalse(tokenResponseDTO.status);
    }


    @After
    public void cleanUp(){
        DTUTokenRepository.clean();
    }
}
