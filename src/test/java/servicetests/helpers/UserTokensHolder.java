package servicetests.helpers;

import token.DTUUserToken;

import java.util.List;

public class UserTokensHolder {
    public int nrOfStartTokens;
    public String name;
    public List<DTUUserToken> userTokens;

    public UserTokensHolder(int nrOfStartTokens, String name) {
        this.nrOfStartTokens = nrOfStartTokens;
        this.name = name;
    }

    public UserTokensHolder() {
    }
}
