FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/TokenManagerService.jar /usr/src
CMD java -Xmx128m -ea -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true -jar TokenManagerService.jar